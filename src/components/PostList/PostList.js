import React from "react";
import PostListItem from "../PostListItem/PostListItem";
import "./PostList.css";

export default function PostList({
  posts,
  onDelete,
  onToggleImportant,
  onToggleLiked,
}) {
  const listItem = posts.map((item) => {
    const { id, ...itemProps } = item;
    return (
      <li key={id} className="list-group-item">
        <PostListItem
          {...itemProps}
          onDelete={() => onDelete(id)}
          onToggleImportant={() => onToggleImportant(id)}
          onToggleLiked={() => onToggleLiked(id)}
        />
      </li>
    );
  });
  return (
    <div>
      <ul className="app-list list-group">{listItem}</ul>
    </div>
  );
}
